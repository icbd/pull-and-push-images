#!/usr/bin/env ruby

# ## Manually Flow
#
# ```sh
# # docker login
# docker pull origin_registry/name:tag
# docker image tag origin_registry/name:tag CONTAINER_REGISTRY/name:tag
# docker push CONTAINER_REGISTRY/name:tag
# ```
#
# ## `config.yaml` example
#
# ```yaml
# images:
#   - dev-ops.gitlab.cn:5050/gitlab/cng-images/gitaly:v13.11.0
#   - dev-ops.gitlab.cn:5050/gitlab/cng-images/gitlab-rails-jh:v13.11.0
# #  - registry_url/image_name:image_tag
# ```

# require 'pry'
require 'yaml'

CONTAINER_REGISTRY = "gitlab-jh.tencentcloudcr.com/cng-images"

config_text = File.open("config.yaml").read
config = YAML.load(config_text)
origin_images = config.fetch("images")

commands = ["#!/bin/sh", ""]
result = []

origin_images.each do |origin_url|
  name_and_tag = origin_url.rpartition("/").last
  target_url = "#{CONTAINER_REGISTRY}/#{name_and_tag}"
  command = [
    "docker pull #{origin_url}",
    "docker image tag #{origin_url} #{target_url}",
    "docker push #{target_url}",
    ""
  ]
  commands << command
  result << target_url
end

File.write("commands.sh", commands.join("\r\n"))
File.write("result.txt", result.join("\r\n"))
