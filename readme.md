## Manually Flow

```sh
# docker login
docker pull origin_registry/name:tag
docker image tag origin_registry/name:tag CONTAINER_REGISTRY/name:tag
docker push CONTAINER_REGISTRY/name:tag
```

## `config.yaml` example

```yaml
images:
  - dev-ops.gitlab.cn:5050/gitlab/cng-images/gitaly:v13.11.0
  - dev-ops.gitlab.cn:5050/gitlab/cng-images/gitlab-rails-jh:v13.11.0
#  - registry_url/image_name:image_tag
```

## How to use

1. Make sure have Registry permission and `docker login` ;
2. Copy `config.yaml.example` to `config.yaml` and edit `images` list;
3. `./generate.rb`
